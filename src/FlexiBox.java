import processing.core.*;

import java.util.ArrayList;


public class FlexiBox extends PApplet {

    public static void main(String args[]) {
        PApplet.main("FlexiBox");
    }

    ArrayList<Collum> c = new ArrayList<Collum>();



    int colls = 50;
    int rows = 40;

    Row r = new Row(rows, height, ((float)height/(float)colls),0);

    @Override
    public void settings() {
        // TODO: Customize screen size and so on here
        size(1024, 1024, P2D);
    }

    @Override
    public void setup() {
        // TODO: Your custom drawing and setup on applet start belongs here
        clear();
        smooth();
        colorMode(HSB,255,100,100);
        r = new Row(rows, height, ((float)height/(float)colls),0);

        for(int i = 0; i < rows; i++){
            c.add( new Collum(colls, width, i* ((float)height/(float)rows), i ));

        }

    }

    @Override
    public void draw() {
        // TODO: Do your drawing for each frame here
        clear();
        background(0);
        float dist = 0.75f;
        for(int i = 0; i < rows-1; i++){
            // c.get(i).arrayBalls[0].Position.y = (height/2.0f)*dist*0.5f + ((1.0f+(float)cos(i*sin(0.0008f*i*0.03f*frameCount)*0.1f+(float)(frameCount)*0.03f))/2.0f)*height*dist ;;
            c.get(i).update(frameCount);

        }
        r.update(frameCount);

        ArrayList<Quad> quad = new ArrayList<Quad>();

        for(int i = 0; i < rows-1; i++){
            Collum cc = c.get(i);
            float y =  r.arrayBalls[i].Position.y;
            float y1 =  r.arrayBalls[i+1].Position.y;
            for(int r = 0; r < cc.numberOfCollums-1; r ++){

                Ball b0 = cc.arrayBalls[r];
                Ball b1 =  cc.arrayBalls[r+1];
                //println("ss"+b0.Position.x);

                quad.add(new Quad(b0.Position.x, y, b1.Position.x-b0.Position.x, y1-y));

            }


        }

        for (int i=0; i<quad.size()-1; i++)
        {

            Quad q = quad.get(i);

            float g = (q.w*q.h/(width*height*0.02f))*255;
            stroke(255-g,20,20);
            fill(g,255);
            rect(q.x, q.y, q.w, q.h);

            //text(q.id,b0.Position.x, height/2);
            // fill(255);
            //  text(index, b0.Position.x, height/2 - 100);
        }

    }
}
