import static java.lang.Math.*;
import static processing.core.PApplet.constrain;
import static processing.core.PApplet.println;

public class Collum {

    Ball[] arrayBalls;
    Spring[] arraySprings;

    int numberOfCollums;
    float size;
    float px;
    int id;

    Collum(int _numberOfCollums, float _size, float _px, int _id){
        numberOfCollums = _numberOfCollums;
        size = _size;
        px = _px;
        id = _id;
        int offSet = 100;
        float w = (float)(size+offSet*2)/(float)(numberOfCollums);

        arrayBalls = new Ball[numberOfCollums];
        arraySprings = new Spring[numberOfCollums-1];

        for (int i=0; i<numberOfCollums; i++)
        {
            arrayBalls[i] = new Ball(w*(i+1)-offSet, this.px /2, w/4, i);
            if (i>0) arraySprings[i-1] = new Spring(arrayBalls[i-1], arrayBalls[i]);
        }

        arrayBalls[0].Fixed = true;
        arrayBalls[numberOfCollums-1].Fixed = true;
    }

     void update(int frameCount){

        int movingBar = numberOfCollums/2;
        float dist = 0.75f;

        float sine = ((1+(float)sin(PI*frameCount*0.002f))*0.5f); // -1 - 1
         float newPos =  sine * (size*dist);
        //if(id==10)println(sine);
       // arrayBalls[movingBar].Position.x = newPos;

        arrayBalls[movingBar].Position.x = (size/2.0f)*dist*0.35f + ((1.0f+(float)cos(id*sin(0.0008f*id*0.3f*frameCount)*0.1f+(float)(frameCount)*0.03f))/2.0f)*size*dist ;

        for (int index=0; index<numberOfCollums-1; index++)
        {
            arraySprings[index].Move();
        }

        for (int index=0; index<numberOfCollums; index++)
        {
            arrayBalls[index].Move();
        }


        int minDistance = 10;
        for (int i=1; i<numberOfCollums-1; i++)
        {
            if(i != movingBar) {
                arrayBalls[i].Position.x = constrain(arrayBalls[i].Position.x, arrayBalls[i-1].Position.x+minDistance, arrayBalls[i+1].Position.x-minDistance);
            }
        }


        minDistance = 0;
        for (int i=1; i<numberOfCollums-1; i++)
        {
            if(i != movingBar) {
                 // arrayBalls[(numberOfCollums-1)-i].Position.x = constrain(arrayBalls[(numberOfCollums-1)-i].Position.x, arrayBalls[((numberOfCollums-1)-i)+1].Position.x+minDistance, arrayBalls[((numberOfCollums-1)-i)].Position.x-minDistance);
            }
        }
    }
}
