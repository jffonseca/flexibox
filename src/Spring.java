import processing.core.*;
class Spring extends PApplet
{
    Ball StartBall;
    Ball EndBall;
    float Stiffness;
    float SlackLength;
    CColor Colour;

    Spring(Ball s, Ball e)
    {
        StartBall = s;
        EndBall = e;
        Stiffness = 0.5f;
        SlackLength = 0.2f*StartBall.Position.dist(EndBall.Position);
        Colour = new CColor(255);
    }

    void Draw()
    {
        stroke(Colour.r,Colour.g,Colour.b,Colour.a);
        strokeWeight(2);
        line(StartBall.Position.x, height-StartBall.Position.y, EndBall.Position.x, height-EndBall.Position.y);
    }

    void Move()
    {
        float extension = StartBall.Position.dist(EndBall.Position) - SlackLength;
        PVector force = PVector.sub(EndBall.Position,StartBall.Position);
        force.setMag(extension*Stiffness);

        StartBall.Force.add(force);
        EndBall.Force.sub(force);

       // StartBall.Position.x = constrain(StartBall.Position.x, StartBall.Position.x,EndBall.Position.x);
       // EndBall.Position.x = constrain(EndBall.Position.x, StartBall.Position.x,EndBall.Position.x);
        // if(StartBall.Position.x>EndBall.Position.x)StartBall.Position.x =StartBall.Position.x;
        //  if(EndBall.Position.x<StartBall.Position.x)EndBall.Position.x =StartBall.Position.x;
    }
}