public class CColor {
    int r;
    int g;
    int b;
    int a;

    CColor(float _r, float _g, float _b, float _a){
        r = (int)_r;
        g = (int)_g;
        b = (int)_b;
        a = (int)_a;
    }

    CColor(float _r, float _g, float _b){
        r = (int)_r;
        g = (int)_g;
        b = (int)_b;
        a = 255;
    }

    CColor(float _c){
        r = (int)_c;
        g = (int)_c;
        b = (int)_c;
        a = 255;
    }

    CColor(float _c, float _a){
        r = (int)_c;
        g = (int)_c;
        b = (int)_c;
        a = (int)_a;
    }
}
