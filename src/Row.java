import static java.lang.Math.*;
import static processing.core.PApplet.constrain;

public class Row {

    Ball[] arrayBalls;
    Spring[] arraySprings;

    int numberOfRows;
    float size;
    float py;
    int id;

    Row(int _numberOfRows, float _size, float _py, int _id){
        numberOfRows = _numberOfRows;
        size = _size;
        py = _py;
        id = _id;
        int offSet = 100;
        float h = (float)(size+offSet*2)/(float)(numberOfRows);

        arrayBalls = new Ball[numberOfRows];
        arraySprings = new Spring[numberOfRows -1];

        for (int i = 0; i< numberOfRows; i++)
        {
            arrayBalls[i] = new Ball(this.py /2, h*(i+1)-offSet, h/4, i);
            if (i>0) arraySprings[i-1] = new Spring(arrayBalls[i-1], arrayBalls[i]);
        }

        arrayBalls[0].Fixed = true;
        arrayBalls[numberOfRows -1].Fixed = true;
    }

    void update(int frameCount){

        int movingBar = numberOfRows /2;
        float dist = 0.75f;
        arrayBalls[movingBar].Position.y = (size/2.0f)*dist*0.25f + (1+(float)sin(1f+(float)(frameCount)*0.015f))/2.0f*size*dist ;
        //   arrayBalls[movingBar].Position.y = (size/2.0f)*dist*0.5f + ((1.0f+(float)cos(id*sin(0.0008f*id*0.03f*frameCount)*0.1f+(float)(frameCount)*0.03f))/2.0f)*size*dist ;
        arrayBalls[movingBar].Colour = new CColor(255, 255, 0);



        for (int index = 0; index< numberOfRows -1; index++)
        {
            arraySprings[index].Move();
        }

        for (int index = 0; index< numberOfRows; index++)
        {
            arrayBalls[index].Move();
        }


        int minDistance = 5;
        for (int i = 1; i< numberOfRows -1; i++)
        {
            if(i != movingBar) {
               arrayBalls[i].Position.y = constrain(arrayBalls[i].Position.y, arrayBalls[i-1].Position.y+minDistance, arrayBalls[i+1].Position.y-minDistance);
            }
        }
    }
}
