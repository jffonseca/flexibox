import processing.core.*;
class Ball extends PApplet
{
    PVector Position;
    PVector Velocity;
    PVector Force;
    CColor Colour;
    float Radius;
    boolean Fixed;
    int id;

    Ball()
    {
        Radius = random(5,50);
        Position = new PVector( random(Radius,width-Radius),random(Radius,height-Radius));
        Velocity = new PVector(random(-10,10),random(-10,10));
        Force = new PVector();
        Colour = new CColor(random(255),random(255),random(255));
        Fixed = false;
    }

    Ball(int r)
    {
        Radius = r;
        Position = new PVector( random(Radius,width-Radius),random(Radius,height-Radius));
        Velocity = new PVector(random(-10,10),random(-10,10));
        Force = new PVector();
        Colour = new CColor(random(255),random(255),random(255));
        Fixed = false;
    }

    Ball(float x, float y)
    {
        Radius = random(5,50);
        Position = new PVector(x,y);
        Velocity = new PVector(random(-10,10),random(-10,10));
        Force = new PVector();
        Colour = new CColor(random(255),random(255),random(255));
        Fixed = false;
    }

    Ball(float x, float y, float r, int _id)
    {
        Radius = r;
        Position = new PVector(x,y);
        id = _id;
        Velocity = new PVector();
        Force = new PVector();
        Colour = new CColor(0,255,255);
        Fixed = false;
    }

    void Draw()
    {
        stroke(Colour.r, Colour.g, Colour.b, Colour.a);
        fill(Colour.r, Colour.g, Colour.b, Colour.a);
        ellipse(Position.x,height-Position.y, 2*Radius,2*Radius);
    }

    void Move()
    {
        if (!Fixed)
        {
            //Force.y -= 1.0;
            PVector Acceleration = PVector.div(Force,Radius);
            Velocity.mult(0.82f);
            Velocity.add(Acceleration);
            Position.add(Velocity);

            if (Position.x>width-Radius) Velocity.x = -Velocity.x;
            if (Position.x<Radius) Velocity.x = -Velocity.x;
            if (Position.y>height-Radius) Velocity.y = -Velocity.y;
            if (Position.y<Radius) Velocity.y = -Velocity.y;
        }
        Force = new PVector();
    }
}